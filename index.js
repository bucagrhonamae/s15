console.log('Hello, World!')

let firstName = 'Rhona'
console.log("First Name: " + firstName)

let lastName = 'Bucag'
console.log("Last Name: " + lastName)

let myAge = 27
console.log("Age: " + myAge)

let hobbies = [ "Hiking", "Singing", "Dancing"];
console.log("Hobbies: ")
console.log(hobbies);

let workAdress = {
	housenumber: "44",
	street: "Ohio",
	city: "Davao",
	state: "Davao del sur"

}

console.log("Work Address:");
console.log(workAdress);

let fullName = 'Steve Rogers'
console.log("My full name is: " + fullName)

let age = 40;
console.log("Age: " + age);
	
let friends = ["Tony", "Bruce", "Thor", "Natasha","Clint", "Nick"];
console.log("My Friends are: ")
console.log(friends);

let profile = {
		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,
};

console.log("My Full Profile: " + profile);
console.log(profile);


let bestFriend = "Bucky Barnes";
console.log("My bestfriend is: " + bestFriend);

const lastLocation = [ "Arctic Ocean" ];
console.log("I was found frozen in: " + lastLocation);
